# My personal website

I decided to use HAML for markup, since I am far too lazy to write HTML,
even if I configure Emmet with Vim and my own keybindings.

To get HAML rendering working, I had to run Ruby on the backend. Rails
is too much overkill for a simple portfolio, so I went with Sinatra
and host it on Heroku.
